from sklearn.neural_network import MLPRegressor
import numpy as np
from analytical_bs import AnalyticalBlackScholes
from random import uniform

class BlackScholesCallDNN:
    def __init__(self, hidden_layers, activation='relu', solver='adam', epochs=10) -> None:
        self.model = MLPRegressor(hidden_layer_sizes=hidden_layers, activation=activation,
         solver=solver, max_iter=epochs, verbose=True)

        self.max_vol = 1.0
        self.min_vol = 0.1

        self.max_risk_free = 0.2
        self.min_risk_free = 0.001
        
        self.max_strike = 50.0
        self.min_strike = 1.0

        self.max_expiration = 1.0
        self.min_expiration = 0.1

        self.max_asset_price = 60.0
        self.min_asset_price = 0.0
        
        self.min_time = 0.0

    def fit(self, data, labels):
        self.model.fit(data, labels)

    def score(self, data, labels):
        return self.model.score(data, labels)
    
    def predict(self, data):
        return self.model.predict(data)
        
    def build_data_set(self, n_vols=10, n_risk_free=10, n_strikes=10, n_expirations=10, n_prices=20, n_ts=10):
        volatilities = np.linspace(self.min_vol, self.max_vol, num=n_vols).tolist()
        risk_frees = np.linspace(self.min_risk_free, self.max_risk_free, num=n_risk_free).tolist()
        strike_prices = np.linspace(self.min_strike, self.max_strike, num=n_strikes).tolist()
        expirations = np.linspace(self.min_expiration, self.max_expiration, num=n_expirations)
        cart = np.array(np.meshgrid(volatilities, risk_frees, strike_prices, expirations)).T.reshape(-1, 4)
        label_data = []
        training_data = []
        for i in range(cart.shape[0]):
            row = cart[i, :]
            bs_model = AnalyticalBlackScholes(*row.tolist())
            asset_prices = np.linspace(self.min_asset_price, self.max_asset_price, num=n_prices)
            expiration = row[3]
            ts = np.linspace(self.min_time, expiration, num=n_ts)
            params = np.array(np.meshgrid(ts, asset_prices)).T.reshape(-1, 2)
            training_data.append(np.hstack((np.tile(row, (n_ts*n_prices, 1)), params)))
            if i % 1000 == 0:
                print("Gemerating Training set Cohort: {}".format(i))
            for j in range(params.shape[0]):
                label_data.append(bs_model.get_call_value(params[j, 0], params[j, 1]))

        data = np.concatenate(training_data, axis=0)
        
        return data, np.array(label_data)

    def generate_random_sample(self):
        vol = uniform(self.min_vol, self.max_vol)
        risk_free = uniform(self.min_risk_free, self.max_risk_free)
        strike = uniform(self.min_strike, self.max_strike)
        expiration = uniform(self.min_expiration, self.max_expiration)
        asset_price = uniform(self.min_asset_price, self.max_asset_price)
        time = uniform(self.min_time, expiration)
        return [vol, risk_free, strike, expiration, time, asset_price]

class BlackScholesPutDNN:
    def __init__(self, hidden_layers, activation='relu', solver='adam', epochs=10) -> None:
        self.model = MLPRegressor(hidden_layer_sizes=hidden_layers, activation=activation,
         solver=solver, max_iter=epochs, verbose=True)

        self.max_vol = 1.0
        self.min_vol = 0.1

        self.max_risk_free = 0.2
        self.min_risk_free = 0.001
        
        self.max_strike = 50.0
        self.min_strike = 1.0

        self.max_expiration = 1.0
        self.min_expiration = 0.1

        self.max_asset_price = 60.0
        self.min_asset_price = 0.0
        
        self.min_time = 0.0

    def fit(self, data, labels):
        self.model.fit(data, labels)

    def score(self, data, labels):
        return self.model.score(data, labels)
    
    def predict(self, data):
        return self.model.predict(data)
        
    def build_data_set(self, n_vols=10, n_risk_free=10, n_strikes=10, n_expirations=10, n_prices=20, n_ts=10):
        volatilities = np.linspace(self.min_vol, self.max_vol, num=n_vols).tolist()
        risk_frees = np.linspace(self.min_risk_free, self.max_risk_free, num=n_risk_free).tolist()
        strike_prices = np.linspace(self.min_strike, self.max_strike, num=n_strikes).tolist()
        expirations = np.linspace(self.min_expiration, self.max_expiration, num=n_expirations)
        cart = np.array(np.meshgrid(volatilities, risk_frees, strike_prices, expirations)).T.reshape(-1, 4)
        label_data = []
        training_data = []
        for i in range(cart.shape[0]):
            row = cart[i, :]
            bs_model = AnalyticalBlackScholes(*row.tolist())
            asset_prices = np.linspace(self.min_asset_price, self.max_asset_price, num=n_prices)
            expiration = row[3]
            ts = np.linspace(self.min_time, expiration, num=n_ts)
            params = np.array(np.meshgrid(ts, asset_prices)).T.reshape(-1, 2)
            training_data.append(np.hstack((np.tile(row, (n_ts*n_prices, 1)), params)))
            if i % 1000 == 0:
                print("Gemerating Training set Cohort: {}".format(i))
            for j in range(params.shape[0]):
                label_data.append(bs_model.get_put_value(params[j, 0], params[j, 1]))

        data = np.concatenate(training_data, axis=0)
        
        return data, np.array(label_data)

    def generate_random_sample(self):
        vol = uniform(self.min_vol, self.max_vol)
        risk_free = uniform(self.min_risk_free, self.max_risk_free)
        strike = uniform(self.min_strike, self.max_strike)
        expiration = uniform(self.min_expiration, self.max_expiration)
        asset_price = uniform(self.min_asset_price, self.max_asset_price)
        time = uniform(self.min_time, expiration)
        return [vol, risk_free, strike, expiration, time, asset_price]
