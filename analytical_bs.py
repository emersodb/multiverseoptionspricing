import math
from scipy.stats import norm

class AnalyticalBlackScholes:
    def __init__(self, volatility, risk_free_rate, strike_price, expiration):
        self.sigma = volatility
        self.r = risk_free_rate
        self.K = strike_price
        self.T = expiration
        self.normal_distribution = norm()

    def compute_ds(self, t, S_t):
        scaling = 1.0/(self.sigma * math.sqrt(self.T - t))
        first_summand = math.log(S_t/self.K)
        second_summand = (self.r + (self.sigma*self.sigma)/2)*(self.T-t)
        d_1 = scaling*(first_summand + second_summand)
        d_2 = d_1 - self.sigma*(self.T - t)
        return (d_1, d_2)

    def compute_discount_factor(self, t):
        return math.exp(-self.r * (self.T - t))

    def get_call_value(self, t, S_t):
        if S_t == 0:
            return 0.0
        elif self.T <= t:
            return max(S_t - self.K, 0.0)
        else:
            d_1, d_2 = self.compute_ds(t, S_t)
            N_d_1 = self.normal_distribution.cdf(d_1)
            N_d_2 = self.normal_distribution.cdf(d_2)
            raw_value = N_d_1*S_t - N_d_2*self.K*self.compute_discount_factor(t)
            return max(raw_value, 0.0)
    
    def get_put_value(self, t, S_t):
        if self.T <= t:
            return max(self.K - S_t, 0)
        elif S_t == 0:
            return (self.K)*self.compute_discount_factor(t)
        else:
            d_1, d_2 = self.compute_ds(t, S_t)
            N_d_1 = self.normal_distribution.cdf(-d_1)
            N_d_2 = self.normal_distribution.cdf(-d_2)
            raw_value = N_d_2*self.K*self.compute_discount_factor(t) - N_d_1*S_t
            return max(raw_value, 0.0)
