import unittest
from dnn_for_bs import BlackScholesCallDNN
from analytical_bs import AnalyticalBlackScholes
import numpy as np

class TestDnnBlackScholes(unittest.TestCase):
    def test_training_shapes(self):
        call_dnn = BlackScholesCallDNN((100, 50), 'relu', 'adam', 15)
        data, labels = call_dnn.build_data_set(3, 3, 3, 3, 3, 3)
        self.assertEqual(data.shape[0], 729)
        self.assertEqual(data.shape[1], 6)
        self.assertEqual(labels.shape[0], 729)

    def test_training_label_values(self):
        call_dnn = BlackScholesCallDNN((100, 50), 'relu', 'adam', 15)
        data, labels = call_dnn.build_data_set(3, 3, 3, 3, 3, 3)

        self.assertAlmostEqual(labels[364], 5.24766, 4)
        self.assertAlmostEqual(data[364, 0], 0.55, 4)
        self.assertAlmostEqual(data[364, 2], 25.5, 4)
        self.assertAlmostEqual(data[364, 3], 0.55, 4)
        self.assertAlmostEqual(data[364, 4], 0.275, 4)



if __name__ == '__main__':
    unittest.main()