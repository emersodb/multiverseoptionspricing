from analytical_bs import AnalyticalBlackScholes
from fd_discretization import FiniteDifferenceDiscretization
from dnn_for_bs import BlackScholesCallDNN
from sklearn.model_selection import train_test_split

import numpy as np
from random import uniform

# Compare FD solutions to Analytical Solution at nodes for small prices

n_samples = 100
n_s = 301
n_t = 500
squared_error = 0.0
absolute_error = 0.0
count = 0

for i in range(n_samples):
    vol = uniform(0.1, 1.0)
    risk_free = uniform(0.001, 0.2)
    strike = uniform(1.0, 50.0)
    expiration = uniform(0.1, 1.0)

    fd_model = FiniteDifferenceDiscretization(vol, risk_free, strike, expiration, 'call')
    bs_model = AnalyticalBlackScholes(vol, risk_free, strike, expiration)
    

    price_matrix, asset_values, times = fd_model.get_prices_at_nodes(n_s, n_t, 300)

    if i % 1 == 0:
        print('Processing Sample: {}'.format(i))

    for j in range(n_s):
        asset_value = asset_values[j]
        if asset_value <= 60:
            for k in range(n_t):
                time_value = times[k]
                fd_pred = price_matrix[j, k]
                analytical_value = bs_model.get_call_value(time_value, asset_value)
                diff = fd_pred - analytical_value
                squared_error += diff*diff
                absolute_error += abs(diff)
                count += 1.0

print('Total Data points: {}'.format(count))
print('MSE: {}'.format((1/float(count)*squared_error)))
print('MAE: {}'.format((1/float(count)*absolute_error)))


call_dnn = BlackScholesCallDNN((50, 50), 'relu', 'adam', 15)
data, labels = call_dnn.build_data_set()

train_data, val_data, train_labels, val_labels = train_test_split(data, labels, test_size=0.2)
call_dnn.fit(train_data, train_labels)
print('R^2 of Predictions: {}'.format(call_dnn.score(val_data, val_labels)))


# Compare DNN preds to Analytical Solution

n_samples = 100000
squared_error = 0.0
absolute_error = 0.0
for i in range(n_samples):
    if i % 10000 == 0:
        print('Processing Sample: {}'.format(i))
    sample = call_dnn.generate_random_sample()
    dnn_pred = call_dnn.predict(np.array([sample]))
    analytical_model = AnalyticalBlackScholes(sample[0], sample[1], sample[2], sample[3])
    analytical_value = analytical_model.get_call_value(sample[4], sample[5])
    diff = dnn_pred - analytical_value
    squared_error += diff*diff
    absolute_error += abs(diff)

print('MSE: {}'.format((1/float(n_samples)*squared_error)))
print('MAE: {}'.format((1/float(n_samples)*absolute_error)))



