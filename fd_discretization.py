import numpy as np
import math

from numpy import linalg

class FiniteDifferenceDiscretization:
    # Using standard implicit forward difference scheme for infinite stability
    # with respect to the time step
    def __init__(self, volatility, risk_free_rate, strike_price, expiration, option_type):
        self.sigma = volatility
        self.r = risk_free_rate
        self.K = strike_price
        self.T = expiration
        self.option_type = option_type
        self.sigma2 = self.sigma*self.sigma

    def discretize_domain(self, n_s, n_t, S_max):
        discrete_asset_values = np.linspace(0, S_max, n_s)
        discrete_times = np.linspace(0, self.T, n_t)
        self.ds = discrete_asset_values[1] - discrete_asset_values[0]
        self.dt = discrete_times[1] - discrete_times[0]
        return discrete_asset_values, discrete_times

    def assemble_fd_matrix(self, aj, bj, cj):
        return np.diag(aj[2:-1], -1) + np.diag(bj[1:-1], 0) + np.diag(cj[1:-2], 1)

    def calculated_diagonals(self, n_s):
        price_indices = np.arange(n_s)

        aj = 0.5*self.dt*(self.r*price_indices - self.sigma2*np.square(price_indices))
        bj = 1 + self.dt*(self.sigma2*np.square(price_indices) + self.r)
        cj = -0.5*self.dt*(self.r*price_indices + self.sigma2*np.square(price_indices))
        return aj, bj, cj

    def construct_boundary_offset(self, a_1, c_n_1, n_s, zero_price, max_price):
        offset = np.zeros((n_s-2,))
        offset[0] = -a_1*zero_price
        offset[-1] = -c_n_1*max_price
        return offset

    def set_boundary_conditions(self, discrete_asset_values, discrete_times, n_s, n_t, S_max):
        prices = np.empty((n_s, n_t))

        if self.option_type == 'call':
            prices[:, -1] = np.maximum(discrete_asset_values - self.K, 0)
            prices[0, :] = 0
            prices[-1, :] = (S_max - self.K)*np.exp(-self.r * discrete_times[::-1])
        else:
            prices[:, -1] = np.maximum(self.K - discrete_asset_values, 0)
            prices[-1, :] = 0
            prices[0, :] = (self.K)*np.exp(-self.r * discrete_times[::-1])

        return prices

    def get_prices_at_nodes(self, n_s, n_t, S_max):
        discrete_asset_values, discrete_times = self.discretize_domain(n_s, n_t, S_max)
        aj, bj, cj = self.calculated_diagonals(n_s)
        fd_matrix = self.assemble_fd_matrix(aj, bj, cj)
        prices = self.set_boundary_conditions(discrete_asset_values, discrete_times, n_s, n_t, S_max)

        for index in range(n_t-2, -1, -1):
            offset = self.construct_boundary_offset(aj[1], cj[-2], n_s, prices[0, index], prices[-1, index])
            prices[1:-1, index] = np.linalg.solve(fd_matrix, prices[1:-1, index+1] + offset)

        return prices, discrete_asset_values, discrete_times


