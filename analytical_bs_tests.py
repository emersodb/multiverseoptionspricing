import unittest
from analytical_bs import AnalyticalBlackScholes

class TestAnalyticalBS(unittest.TestCase):
    def test_boundary_call_prices(self):
        model = AnalyticalBlackScholes(0.2, 0.05, 30, 1.0)
        self.assertEqual(model.get_call_value(1.0, 45), 15.0)
        self.assertEqual(model.get_call_value(1.0, 29), 0.0)
        self.assertEqual(model.get_call_value(0.5, 0.0), 0.0)
    
    def test_boundary_put_prices(self):
        model = AnalyticalBlackScholes(0.2, 0.05, 30, 1.0)
        self.assertEqual(model.get_put_value(1.0, 35), 0.0)
        self.assertEqual(model.get_put_value(1.0, 28), 2.0)
        self.assertAlmostEqual(model.get_put_value(0.2, 0), 28.8237, 4)

    def test_interior_call_price(self):
        model = AnalyticalBlackScholes(0.2, 0.05, 100, 1.0)
        self.assertAlmostEqual(model.get_call_value(0.0, 85), 3.2136 , 4)
        self.assertAlmostEqual(model.get_call_value(0.0, 115), 21.7905, 4)

    def test_interior_put_prices(self):
        model = AnalyticalBlackScholes(0.2, 0.05, 100, 1.0)
        self.assertAlmostEqual(model.get_call_value(0.1, 85), 2.4995 , 4)
        self.assertAlmostEqual(model.get_call_value(0.1, 115), 20.8860, 4)


if __name__ == '__main__':
    unittest.main()