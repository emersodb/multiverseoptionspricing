import unittest
from fd_discretization import FiniteDifferenceDiscretization

class TestFiniteDifferences(unittest.TestCase):
    def test_discretizers(self):
        fd_call_model = FiniteDifferenceDiscretization(0.2, 0.05, 100, 1, 'call')
        asset_values, times = fd_call_model.discretize_domain(11, 21, 100)
        self.assertEqual(asset_values[0], 0)
        self.assertEqual(asset_values[10], 100)
        self.assertEqual(asset_values[2], 20)
        self.assertEqual(times[0], 0)
        self.assertEqual(times[20], 1.0)
        self.assertEqual(times[2], 0.1)

    def test_boundary_condition_values(self):
        n_s = 11
        n_t = 21
        S_max = 400
        fd_call_model = FiniteDifferenceDiscretization(0.2, 0.05, 100, 1, 'call')
        fd_put_model = FiniteDifferenceDiscretization(0.2, 0.05, 100, 1, 'put')
        assets_call, times_call = fd_call_model.discretize_domain(n_s, n_t, S_max)
        assets_put, times_put = fd_put_model.discretize_domain(n_s, n_t, S_max)
        prices_call = fd_call_model.set_boundary_conditions(assets_call, times_call, n_s, n_t, S_max)
        prices_put = fd_put_model.set_boundary_conditions(assets_put, times_put, n_s, n_t, S_max)

        self.assertAlmostEqual(prices_call[-1, -1], 300, 4)
        self.assertAlmostEqual(prices_call[-2, -1], 260, 4)
        self.assertAlmostEqual(prices_call[0, 5], 0.0, 4)
        self.assertAlmostEqual(prices_call[0, 7], 0.0, 4)
        self.assertAlmostEqual(prices_call[-1, -2], 299.2509, 4)

        self.assertAlmostEqual(prices_put[-1, -1], 0, 4)
        self.assertAlmostEqual(prices_put[1, -1], 60, 4)
        self.assertAlmostEqual(prices_put[0, 0], 95.1229, 4)
        self.assertAlmostEqual(prices_put[0, -2], 99.7503, 4)
        self.assertAlmostEqual(prices_put[-1, -2], 0.0, 4)

    def test_discretized_matrix(self):
        n_s = 101
        fd_call_model = FiniteDifferenceDiscretization(0.2, 0.05, 100, 1, 'call')
        _, _, = fd_call_model.discretize_domain(n_s, 101, 400)
        aj, bj, cj = fd_call_model.calculated_diagonals(n_s)
        fd_matrix = fd_call_model.assemble_fd_matrix(aj, bj, cj)
        self.assertAlmostEqual(fd_matrix[0, 0], 1.0009, 4)
        self.assertAlmostEqual(fd_matrix[1, 0], -0.0003, 4)
        self.assertAlmostEqual(fd_matrix[0, 1], -0.00045, 5)
        self.assertAlmostEqual(fd_matrix[-1, -1], 4.9209, 4)
        self.assertAlmostEqual(fd_matrix[-1, -2], -1.93545, 4)
        self.assertAlmostEqual(fd_matrix[-2, -4], 0.0, 4)

    def test_linear_system_shapes(self):
        n_s = 100
        n_t = 200
        S_max = 400
        fd_call_model = FiniteDifferenceDiscretization(0.2, 0.05, 100, 1, 'call')
        asset_values, times = fd_call_model.discretize_domain(n_s, n_t, S_max)
        prices = fd_call_model.set_boundary_conditions(asset_values, times, n_s, n_t, S_max)
        self.assertEqual(prices.shape[0], n_s)
        self.assertEqual(prices.shape[1], n_t)
        aj, bj, cj = fd_call_model.calculated_diagonals(n_s)
        fd_matrix = fd_call_model.assemble_fd_matrix(aj, bj, cj)
        self.assertEqual(fd_matrix.shape[0], n_s-2)
        self.assertEqual(fd_matrix.shape[1], n_s-2)



if __name__ == '__main__':
    unittest.main()